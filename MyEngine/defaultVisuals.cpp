#include "defaultVisuals.h"

class Cursor3d{
public:
	bool bIs3dCursorActive;
};

void drawCylinderX(float scale){
	glColor3f(255.0f,0.0f,0.0f);
	glLoadIdentity();
	GLUquadric *quad = gluNewQuadric();
	
	glRotatef(100.0f,0.0f,1.0f,0.0f);
	glTransformf(10.0f,0.0f,0.0f);
	gluCylinder(quad,0.5f*scale,0.0f,1.0f*scale*1.3,8,8);
	//glFlush();
}

void draw3dCursor(){
	//glLoadIdentity();
	glColor3f(127.5f,127.5f,0);
	glRectf(0.0f,0.0f,0.045f,0.045f);

	glColor3f(255.0f,0.0f,0.0f);//red
	glRectf(0.0f,0.003f,0.09f,-0.003f);//(x1,y1),(x2,y2)-->RED
	
	/glBegin(GL_TRIANGLES);//GL_POINTS,
 //                    GL_LINES,
 //                    GL_LINE_STRIP,
 //                    GL_LINE_LOOP,
 //                    GL_TRIANGLES,
 //                    GL_TRIANGLE_STRIP,
 //                    GL_TRIANGLE_FAN,
 //                    GL_QUADS,
 //                    GL_QUAD_STRIP,
 //                    GL_POLYGON
		glVertex2f(0.09f,0.01f);
		glVertex2f(0.09f,-0.01f);
		glVertex2f(0.11f,0.002f);
	glEnd();*/

	glColor3f(0.0f,255.0f,0.0f);//green
	glRectf(-0.003f,0.0f,0.003f,0.11f);//(x1,y1),(x2,y2)-->GREEN

	/*glBegin(GL_TRIANGLES);
		glVertex2f(0.01f,0.11f);
		glVertex2f(-0.01f,0.11f);
		glVertex2f(0.002f,0.13f);
	glEnd();*/

	drawCylinderX(0.04f);
	//glColor3d(0,0,255)//blue
}


