#include <iostream>

#define SIZE 4

using namespace std;

void scram(char board[SIZE]){
	char newBoard[SIZE] = {0};
	int i ,j;
	for(j = 0,i = 0; j < SIZE; j++){
		newBoard[i] = board[j];
		i++;
	}
	cout <<newBoard<<endl;
}

int main(int argc , char *argv[]){
	char board[SIZE] = {0};
	char *newBoard = NULL;
	int i = 0;
	for(i = 0; i < SIZE; i++){
		cout <<"Give the board:";
		cin >> board[i];
		cout <<"board[" <<i<<"]:"<<board[i]<<endl;
	}

	scram(board);

	return 0;
}
