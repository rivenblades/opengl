//http://gamedev.stackexchange.com/questions/42187/where-to-start-learning-opengl-with-c
#include <iostream>
#include <stdio.h>
#include <GL/glu.h>
#include <GL/glut.h>
//#include <X11/Xlib.h>

#include "defaultVisuals.h"/* 3D Cursor*/
#include "Tool.h"/* 3D Cursor Class,*/

void setup(){
	static float c = 0.2f;
	glClearColor(c,c,c,c);/* clear the buffer with black color*/
	
}

void render(){
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);/* GL_COLOR_BUFFER_BIT,GL_DEPTH_BUFFER_BIT,GL_ACCUM_BUFFER_BIT,GL_STENCIL_BUFFER_BIT*/
	/* all drawings go here-->*/
	//-->
	//glColor3d(0,0,0);
	//glRectf(-0.75,0.75,0.75,-0.75);
	draw3dCursor();
		//<--
	glutSwapBuffers();
}

int main(int argc , char **argv){
	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);/* set the display mode*///--GLUT_RGBA,GLUT_SINGLE,GLUT_DOUBLE,GLUT_DEPTH,GLUT_ALPHA,GLUT_STENCIL
	glutInitWindowSize(800,600);/* set window size proportions*/
	glutInitWindowPosition(200,100);
	glutCreateWindow("Hello 3d Engine");/* create the actual window with a Title*/

	setup();//setting up

	Tool cursor = Tool(0.1f,0.2f);
	cursor.setActive(true);
	printf("x is :%f\ny is :%f\n" , cursor.getX(), cursor.getY());

	glutDisplayFunc(render);//rendering the window

	glutMainLoop();

	return 0;
}