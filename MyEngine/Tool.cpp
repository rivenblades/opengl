#include "Tool.h"

Tool::Tool(float x, float y){
	_x = x;
	_y = y;
}

bool Tool::getActive( void ){
	return isActive;
}

void Tool::setActive(bool newState){
	isActive = newState;
}

float Tool::getX(){
	return _x;
}

float Tool::getY(){
	return _y;
}