#ifndef TOOL_H
#define TOOL_H

class Tool{
	private://by default they are private
	float _x;
	float _y;

	public:
	Tool(float x, float y);//Constructor
	bool isActive;
	bool getActive( void );
	void setActive(bool newState);

	float getX();
	float getY();
};

#endif